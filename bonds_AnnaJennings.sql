-- Bond Portfolio
--Q1
select * from bond
where CUSIP = '28717RH95'

--Q2
select * from bond
order by maturity asc
where 0=0

--Q3
select sum(quantity * price) as 'value of bond'
from bond
where 0=0

--Q4
select id, (quantity * coupon/100) as 'annual return of bond'
from bond
where 0=0

--Q5
select * from bond b 
join rating r on r.rating = b.rating 
where r.ordinal < 4
order by id asc

--Q6
select avg(price) as 'Average price', avg(coupon) as 'Average coupon'
from bond
where 0=0
group by id

--Q7
select b.id, (coupon/price) as overpriced, r.expected_yield 
from bond b 
join rating r on r.rating = b.rating 
where (coupon/price) < r.expected_yield
order by id asc
