select * from trade
select * from position
select * from price_point

-- Trading History

--Q1
select t.*
from trade t
join position p on p.opening_trade_id = t.id or p.closing_trade_id = t.id
where p.trader_id = 1 and t.stock = 'MRK'

--Q2
/*Find the total profit or loss for a given trader over the day, 
as the sum of the product of trade size and price for all sales, 
minus the sum of the product of size and price for all buys.*/

-- Initial attempt
--select ((sum(size * price) where buy = 0) - (sum(size * price) where buy = 1))
--from trade 

select [sell sum] - [buy sum] from (select sum(size*price) as [sell sum] from trade where buy = 0) as sales, (select sum(size*price) as [buy sum] from trade where buy = 1) as buys, trade t
join position p on p.opening_trade_id = t.id or p.closing_trade_id = t.id
where p.trader_id = 1 
and t.stock = 'MRK'
and p.closing_trade_id != p.opening_trade_id
and p.closing_trade_id is not null;

--Q3